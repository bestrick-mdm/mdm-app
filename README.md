# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Install Postgress ###

* Install Postgress
* Install pgadmin --> http://127.0.0.1:55865/browser/#


### Install Kafka

* https://kafka.apache.org/quickstart
* cd /Users/anuragsaran/Documents/mw/mdm/kafka_2.12-2.5.0
* bin/zookeeper-server-start.sh config/zookeeper.properties
* bin/kafka-server-start.sh config/server.properties
* bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic customer

### Install ElasticSearch : https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.htmlini

* https://www.elastic.co/downloads/elasticsearch
* Run bin/elasticsearch 


### Install Infinispan

* bin/server.sh

### Build/Run App

Build 	:- mvn clean package

Debug 	:- mvn compile quarkus:dev -Ddebug=port {5006 default:5005}

Running :- java -Dquarkus-profile=dev -jar *-runner.jar

