/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.repo;

import javax.enterprise.context.ApplicationScoped;

import com.mdm.app.entity.CustomerMaster;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

/**
 * This class CustomerMasterRepository.
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerMasterRepository implements PanacheRepository<CustomerMaster> {

	/**
	 * Search Customer Master Data by customer name.
	 * 
	 * @param name {@link String}
	 * @return {@link CustomerMaster}
	 */
	public CustomerMaster findByName(String name) {
		return find("name", name).firstResult();
	}
}
