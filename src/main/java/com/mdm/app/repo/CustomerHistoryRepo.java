/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.repo;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import com.mdm.app.entity.CustomerHistory;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

/**
 * This class CustomerHistoryRepo.
 * 
 * @author vickyrajput
 */
@ApplicationScoped
@Transactional
public class CustomerHistoryRepo implements PanacheRepository<CustomerHistory> {

	/**
	 * Search Customer Master Data by customer name.
	 * 
	 * @param  {@link String}
	 * @return {@link CustomerHistory}
	 */
	public CustomerHistory findByCustomerId(String customerId) {
		return find("customerId", customerId).firstResult();
	}
}
