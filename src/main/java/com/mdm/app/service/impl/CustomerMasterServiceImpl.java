/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service.impl;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.modelmapper.ModelMapper;

import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.Status;
import com.mdm.app.dto.CustomReponseStatus;
import com.mdm.app.dto.CustomerDataReport;
import com.mdm.app.dto.CustomerValueDTO;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerData;
import com.mdm.app.entity.CustomerHistory;
import com.mdm.app.entity.CustomerIdMapper;
import com.mdm.app.entity.CustomerMaster;
import com.mdm.app.entity.CustomerMasterHistory;
import com.mdm.app.repo.CustomerHistoryRepo;
import com.mdm.app.repo.CustomerMasterRepository;
import com.mdm.app.scheduler.DataReportScheduler;
import com.mdm.app.service.ICustomerMasterService;

import io.quarkus.infinispan.client.Remote;
import io.quarkus.panache.common.Parameters;
import io.vertx.axle.core.eventbus.EventBus;

/**
 * This class CustomerMasterServiceImpl implements
 * {@link ICustomerMasterService}
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerMasterServiceImpl implements ICustomerMasterService {
	private final static Logger LOGGER = Logger.getLogger(CustomerMasterServiceImpl.class.getName());
	@Inject
	CustomerMasterRepository customerRepository;
	@Inject
	CustomerHistoryRepo customerHistoryRepo;
	@Inject
	EntityManager em;
	@Inject
	EventBus bus;
	@Inject
	RemoteCacheManager remoteCacheManager;
	@Inject
	@Remote(value = "dashboard")
	RemoteCache<String, String> cache;
	@Inject
	ErrorBean errorBean;

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse listCustomer() {
		LOGGER.info("Calling APi");
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			List<CustomerMaster> customers = customerRepository.listAll();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("totalSize", customers.size());
			response.put("AllData", customers);

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "listCustomer");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		LOGGER.info(serviceResponse.toString());
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse getCustomerById(String customerId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			LOGGER.info("customer id " + customerId);
			Optional<CustomerMaster> customer = customerRepository
					.find("customerId =:customerId", Parameters.with("customerId", customerId)).singleResultOptional();
			LOGGER.info("response result " + customer);
			if (customer.isPresent()) {
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
				response.put("data", customer.get());
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerById");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse createCustomer(CustomerMaster customer) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			customer.setCreatedDate(LocalDateTime.now());
			customerRepository.persist(customer);
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "createCustomer");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse updateCustomer(Long id, CustomerMaster customer) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerMaster customer2 = customerRepository.findById(id);
			if (customer2 != null) {
				// customerRepository.updateCustomer(mapOnlyChangeValue(customer2, customer));
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);

			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "updateCustomer");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse deleteCustomer(Long id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerMaster customer = customerRepository.findById(id);
			if (customer != null) {
				customerRepository.delete(customer);
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "deleteCustomer");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse getCustomerByNameOrAddress(String parameter) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			SearchSession searchSession = Search.session(em);
			List<CustomerMaster> customers = searchSession.search(CustomerMaster.class)
					.where(f -> f.bool().should(f.match().field("firstName").matching(parameter).fuzzy())
							.should(f.match().field("lastName").matching(parameter).fuzzy())
							.should(f.match().field("address.street_1").matching(parameter).fuzzy())
							.should(f.match().field("address.street_2").matching(parameter).fuzzy())
							.should(f.match().field("address.city").matching(parameter).fuzzy())
							.should(f.match().field("address.state").matching(parameter).fuzzy())
							.should(f.match().field("address.country").matching(parameter).fuzzy()))
					.fetchAllHits();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("totalSize", customers.size());
			response.put("AllData", customers);

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerByNameOrAddress");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse getCustomerDataReport(String tablename, String fieldname) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			String customerDataReport = cache.get(tablename + "-" + fieldname);
			LOGGER.info("Data Report :-" + customerDataReport);
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			if (customerDataReport == null) {
				CustomerDataReport dataReport = getCustomerDataReportCache(tablename, fieldname);
				LOGGER.info("Data Report with Table :-" + dataReport);
				response.put("data", dataReport);
			} else {
				response.put("data", customerDataReport);
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerDataReport");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Transactional(value = TxType.REQUIRED)
	@Override
	public CustomerDataReport getCustomerDataReportCache(String tablename, String fieldname) {
		CustomerDataReport customerDataReport = new CustomerDataReport();
		try {
			String nullQuery = "SELECT\r\n" + "	SUM(CASE WHEN " + fieldname
					+ " IS NULL THEN 1 ELSE 0 END) AS nullVal,\r\n" + "	count(distinct " + fieldname
					+ ") as uniqueval,\r\n" + "	count(" + fieldname + ") - count(distinct " + fieldname
					+ ") as nonunique\r\n" + ", count(" + fieldname + ") as total from " + tablename + " ";

			String groupByQuery = "SELECT " + fieldname + ", COUNT(id),LENGTH(CAST(" + fieldname
					+ " AS varchar(100)))  FROM " + tablename + " GROUP BY " + fieldname;
			String dataTypeQuery = "select \r\n" + "	SUM(CASE WHEN " + fieldname
					+ " ~ '^((\\d+)+(\\.\\d+))$' THEN 1 ELSE 0 END) AS decimal_,\r\n" + "	SUM(CASE WHEN " + fieldname
					+ " ~ '^[a-zA-Z ]*$' THEN 1 ELSE 0 END) AS alpha, \r\n" + "	SUM(CASE WHEN " + fieldname
					+ " ~ '^-?(0|[1-9]\\d*)$' THEN 1 ELSE 0 END) AS int,\r\n" + "	SUM(CASE WHEN " + fieldname
					+ " ~ '^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$' THEN 1 ELSE 0 END) AS date_\r\n" + "from "
					+ tablename;

			Query query = em.createNativeQuery(nullQuery);
			List<Object> list = query.getResultList();
			Object[] integers = (Object[]) list.get(0);
			LOGGER.info(integers.length + "Data :" + list.toString());
			for (int i = 0; i < integers.length; i++) {
				if (i == 0) {
					customerDataReport.setNullCount(((BigInteger) integers[i]).intValue());
				} else if (i == 1) {
					customerDataReport.setUniqueCount(((BigInteger) integers[i]).intValue());
				} else if (i == 2) {
					customerDataReport.setNonUniqueCount(((BigInteger) integers[i]).intValue());
				} else {
					customerDataReport.setTotalRecords(((BigInteger) integers[i]).intValue());
				}
			}
			customerDataReport.setNullPer(
					((float) customerDataReport.getNullCount() / customerDataReport.getTotalRecords()) * 100);
			customerDataReport.setUniquePer(
					((float) customerDataReport.getUniqueCount() / customerDataReport.getTotalRecords()) * 100);
			customerDataReport.setNonUniquePer(
					((float) customerDataReport.getNonUniqueCount() / customerDataReport.getTotalRecords()) * 100);
			query = em.createNativeQuery(groupByQuery);
			list = query.getResultList();
			for (int i = 0; i < list.size(); i++) {
				Object[] objects = (Object[]) list.get(i);
				CustomerValueDTO valueDTO = new CustomerValueDTO();
				for (int j = 0; j < objects.length; j++) {
					if (j == 0) {
						valueDTO.setValue((String) objects[j]);
					} else if (j == 1) {
						valueDTO.setFrequency((BigInteger) objects[j]);
					} else {
						valueDTO.setLength((Integer) objects[j]);
					}
				}
				customerDataReport.getValue().add(valueDTO);
			}
			query = em.createNativeQuery(dataTypeQuery);
			list = query.getResultList();
			Object[] dataTypes = (Object[]) list.get(0);
			LOGGER.info(dataTypes.length + "Data :" + list.toString());
			for (int i = 0; i < dataTypes.length; i++) {
				if (i == 0) {
					customerDataReport.setDecimal((BigInteger) dataTypes[i]);
				} else if (i == 1) {
					customerDataReport.setString((BigInteger) dataTypes[i]);
				} else if (i == 2) {
					customerDataReport.setInteger((BigInteger) dataTypes[i]);
				} else {
					customerDataReport.setDate((BigInteger) dataTypes[i]);
				}
			}
			LOGGER.info("Data Report :-" + customerDataReport.toString());
		} catch (Exception e) {
			// errorBean.createError(e, this.getClass().getName(),
			// "getCustomerDataReportCache");
			LOGGER.info(e.getMessage());
		}
		return customerDataReport;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponse getAllColumnByTableName(String tableName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			String columnNameQuery = "SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name = ";
			String columnQuery = columnNameQuery + "'" + tableName + "'";
			Query query = em.createNativeQuery(columnQuery);
			List<String> columnlist = query.getResultList();
			columnlist.removeAll(Arrays.asList(DataReportScheduler.COLUMN_NAME));
			columnlist.removeIf(s -> s.contains("_rule"));
			columnlist.removeIf(s -> s.contains("_source"));
			columnlist.removeIf(s -> s.contains("_count"));
			LOGGER.info("Data Report :-" + columnlist.toString());
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("Alldata", columnlist);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerDataReport");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse createCustomerByProcess(Long customerTableId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerData customerData = new CustomerData();
			CustomerHistory customer = CustomerHistory.findById(customerTableId);
			customerData.setCustomer(customer);
			LOGGER.info(customerData.toString());

			CustomerMaster customerMaster = new CustomerMaster();
			ModelMapper mapper = new ModelMapper();
			mapper.map(customerData.getCustomer(), customerMaster);
			LOGGER.info("After Mapper successfully ! " + customerMaster);
			customerMaster.setCreatedDate(LocalDateTime.now());
			customerMaster.persistAndFlush();
			LOGGER.info("Insert Master successfully ! " + customerMaster);
			CustomerMasterHistory masterHistory = new CustomerMasterHistory();
			mapper.map(customerMaster, masterHistory);
			masterHistory.setVersion(1);
			masterHistory.persistAndFlush();
			LOGGER.info("Insert Master History successfully ! " + masterHistory);
			CustomerIdMapper customerIdMapper = new CustomerIdMapper();
			customerIdMapper.setPrimaryCustomerId(customerData.getCustomer().getCustomerId());
			customerIdMapper.setSecondaryCustomerId(customerData.getCustomer().getCustomerId());
			customerIdMapper.setSecondarySource(customerData.getCustomer().getFromSource());
			customerIdMapper.persistAndFlush();
			LOGGER.info("Create New Customer Map Customer Id successfully ! ");

			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "createCustomer");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}
}
