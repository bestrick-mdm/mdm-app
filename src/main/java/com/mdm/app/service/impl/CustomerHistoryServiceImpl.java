/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service.impl;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.apache.commons.lang3.StringUtils;

import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.Status;
import com.mdm.app.dto.CustomReponseStatus;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerData;
import com.mdm.app.entity.CustomerHistory;
import com.mdm.app.queue.consumer.CustomerQueueConsumer;
import com.mdm.app.service.ICustomerHistoryService;

import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;

/**
 * This class CustomerHistoryServiceImpl implements
 * {@link ICustomerHistoryService}
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerHistoryServiceImpl implements ICustomerHistoryService {
	@Inject
	CustomerQueueConsumer customerQueueConsumer;

	@Inject
	ErrorBean errorBean;
	private final static Logger LOGGER = Logger.getLogger(CustomerHistoryServiceImpl.class.getName());

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServiceResponse listCustomerHistory() {
		LOGGER.info("Calling APi");
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			List<CustomerHistory> customers = CustomerHistory.findAll().list();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("totalSize", customers.size());
			response.put("AllData", customers);

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "listCustomerHistory");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		LOGGER.info(serviceResponse.toString());
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse getCustomerHistoryById(Long id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerHistory customer = CustomerHistory.findById(id);
			if (customer != null) {
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
				response.put("data", customer);
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerHistoryById");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse createCustomerHistory(CustomerData customerData) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			LOGGER.info(customerData.toString());
			customerData.getCustomer().setCreatedDate(LocalDateTime.now());
			customerData.getCustomer().setUpdatedDate(LocalDateTime.now());
			customerData.getCustomer().persistAndFlush();
			customerQueueConsumer.processCustomerData(customerData);
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "createCustomerHistory");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServiceResponse processCustomerHistory(Long customerHistoryId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerData customerData = new CustomerData();
			CustomerHistory customer = CustomerHistory.findById(customerHistoryId);
			customerData.setCustomer(customer);
			LOGGER.info(customerData.toString());
			customerQueueConsumer.startProcessingCustomerData(customerData);
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "processCustomerHistory");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	@Override
	public ServiceResponse getCustomerHistoryById(String id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			Optional<CustomerHistory> customer = null;
			if (StringUtils.isAlphanumeric(id)) {
				customer = CustomerHistory
						.find("customerId =:customerId ", Sort.descending("id"), Parameters.with("customerId", id))
						.firstResultOptional();
			} else {
				customer = CustomerHistory.findByIdOptional(id);
			}
			if (customer.isPresent()) {
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
				response.put("data", customer.get());
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerHistoryById");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}
}
