/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service.impl;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.Status;
import com.mdm.app.dto.CustomReponseStatus;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerRules;
import com.mdm.app.service.ICustomerRuleService;

/**
 * This class CustomerRuleServiceImpl implements {@link ICustomerRuleService}
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerRuleServiceImpl implements ICustomerRuleService {

	private final static Logger LOGGER = Logger.getLogger(CustomerRuleServiceImpl.class.getName());

	@Inject
	ErrorBean errorBean;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServiceResponse listCustomerRule() {
		System.out.println("Calling APi");
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			List<CustomerRules> customerRules = CustomerRules.findAll().list();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("totalSize", customerRules.size());
			response.put("AllData", customerRules);

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "listCustomerRule");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		LOGGER.info(serviceResponse.toString());
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse getCustomerRuleById(Long id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerRules customerRules = CustomerRules.findById(id);
			if (customerRules != null) {
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
				response.put("data", customerRules);
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerRuleById");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse createCustomerRule(CustomerRules customerRules) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			customerRules.setCreatedDate(LocalDateTime.now());
			customerRules.setUpdatedDate(LocalDateTime.now());
			customerRules.persistAndFlush();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "createCustomerRule");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse updateCustomerRule(Long id, CustomerRules customerRules) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			customerRules.setUpdatedDate(LocalDateTime.now());
			CustomerRules customerRules2 = CustomerRules.findById(id);
			if (customerRules2 != null) {
				updateData(customerRules, customerRules2);
				customerRules2.persistAndFlush();
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);

			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "updateCustomerRule");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRED)
	@Override
	public ServiceResponse deleteCustomerRule(Long id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			if (id > 0) {
				CustomerRules customerRules = CustomerRules.findById(id);
				customerRules.delete();
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "deleteCustomerRule");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}
	
	private CustomerRules updateData(CustomerRules sou,CustomerRules des) {
		des.setFieldName(sou.getFieldName());
		des.setFromSource(sou.getFromSource());
		des.setMatchingWeight(sou.getMatchingWeight());
		des.setPriority(sou.getPriority());
		des.setRuleName(sou.getRuleName());
		des.setUpdatedBy(sou.getUpdatedBy());
		des.setUpdatedDate(LocalDateTime.now());
		return des;
		
	}

}
