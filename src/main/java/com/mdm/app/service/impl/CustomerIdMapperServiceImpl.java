/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service.impl;

import java.util.LinkedHashMap;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.Status;
import com.mdm.app.dto.CustomReponseStatus;
import com.mdm.app.dto.CustomerIDMapperDTO;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerIdMapper;
import com.mdm.app.service.ICustomerIdMapperService;

/**
 * This class CustomerIdMapperServiceImpl implements
 * {@link ICustomerIdMapperService}
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerIdMapperServiceImpl implements ICustomerIdMapperService {
	private final static Logger LOGGER = Logger.getLogger(CustomerIdMapperServiceImpl.class.getName());

	@Inject
	ErrorBean errorBean;
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse createCustomerIdMapper(CustomerIDMapperDTO customerIDMapperDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			LOGGER.info(customerIDMapperDTO.toString());
			CustomerIdMapper customerIdMapper = new CustomerIdMapper();
			customerIdMapper.setPrimaryCustomerId(customerIDMapperDTO.getCustomerId());
			customerIdMapper.setSecondaryCustomerId(customerIDMapperDTO.getMapCustomerId());
			customerIdMapper.persistAndFlush();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "createCustomerIdMapper");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}
}
