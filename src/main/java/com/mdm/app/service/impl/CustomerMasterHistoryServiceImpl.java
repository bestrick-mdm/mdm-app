/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.Status;
import com.mdm.app.dto.CustomReponseStatus;
import com.mdm.app.dto.CustomerDTO;
import com.mdm.app.dto.CustomerDataWithAllSources;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerDataSources;
import com.mdm.app.entity.CustomerHistory;
import com.mdm.app.entity.CustomerIdMapper;
import com.mdm.app.entity.CustomerMaster;
import com.mdm.app.service.ICustomerMasterHistoryService;

import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;

/**
 * This class CustomerMasterHistoryServiceImpl implements
 * {@link ICustomerMasterHistoryService}
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerMasterHistoryServiceImpl implements ICustomerMasterHistoryService {
	private final static Logger LOGGER = Logger.getLogger(CustomerMasterHistoryServiceImpl.class.getName());

	@Inject
	ErrorBean errorBean;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServiceResponse listCustomer() {
		LOGGER.info("Calling APi");
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			List<CustomerDTO> customerDTOs = new ArrayList<>();
			List<CustomerMaster> customers = CustomerMaster.listAll();
			// Create Conversion Type
			Type listType = new TypeToken<List<CustomerDTO>>() {
			}.getType();
			// Convert List of Entity objects to a List of DTOs objects
			customerDTOs = new ModelMapper().map(customers, listType);
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("totalSize", customerDTOs.size());
			response.put("AllData", customerDTOs);

		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "listCustomer");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		LOGGER.info(serviceResponse.toString());
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServiceResponse getCustomerById(String customerId) {
		LOGGER.info("Calling CustomerMasterHistoryServiceImpl APi " + customerId);
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			ModelMapper mapper = new ModelMapper();
			CustomerDataWithAllSources allSources = new CustomerDataWithAllSources();
			CustomerMaster customerMaster = CustomerMaster
					.find("customerId =:customerId", Parameters.with("customerId", customerId)).firstResult();
			List<CustomerIdMapper> idMappers = CustomerIdMapper
					.find("secondaryCustomerId = :secondaryCustomerId ", Parameters.with("secondaryCustomerId", customerId))
					.list();
			List<String> customerids = new ArrayList<>();
			idMappers.forEach(cus -> customerids.add(cus.getPrimaryCustomerId()));
			LOGGER.info("customer Ids Map :- " + customerids.toString());
			List<CustomerHistory> customers = CustomerHistory
					.find("customerId in ( :customerId )",Sort.descending("id") ,Parameters.with("customerId", customerids)).list();
			
			if (customerMaster != null) {
				List<CustomerDTO> customerDTOs = new ArrayList<>();
				// Create Conversion Type
				Type listType = new TypeToken<List<CustomerDTO>>() {
				}.getType();
				// Convert List of Entity objects to a List of DTOs objects
				LOGGER.info("customer data :- " + customerMaster);
				LOGGER.info("customer data :- " + customers);
				customerDTOs = mapper.map(customers, listType);
				
				LOGGER.info("customer Map  :- " + customerDTOs.toString());
				customerDTOs.forEach((cus) -> {
					Optional<CustomerDataSources> customerDataSources = CustomerDataSources
							.find("sourceName =:sourceName", Parameters.with("sourceName", cus.getFromSource()))
							.singleResultOptional();
					if (customerDataSources.isPresent()) {
						CustomerDataSources customerDataSource = customerDataSources.get();
						cus.setFromSourceColor(customerDataSource.getColorName());
					} else {
						cus.setFromSourceColor("yellow");
					}
				});
				LOGGER.info("After MApp List customer :- " + customers.toString());
				allSources.setCustomerMasterDTO(customerMaster);
				allSources.setCustomerHistoryDTOs(customerDTOs);
			}
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("data", allSources);
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "getCustomerById");
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		LOGGER.info(serviceResponse.toString());
		return serviceResponse;
	}

}
