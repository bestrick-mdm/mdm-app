package com.mdm.app.service.impl;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.mdm.app.constant.Status;
import com.mdm.app.dto.CustomReponseStatus;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerDataSources;
import com.mdm.app.entity.CustomerHistory;
import com.mdm.app.service.ICustomerDatasourceService;

/**
 * This class CustomerDataSourceServiceImpl implements
 * {@link ICustomerDatasourceService}
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerDataSourceServiceImpl implements ICustomerDatasourceService {

	private final static Logger LOGGER = Logger.getLogger(CustomerDataSourceServiceImpl.class.getName());

	/**
	 * {@inheritDoc}
	 */
	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse createCustomerDataSource(CustomerDataSources customerDataSources) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			customerDataSources.persistAndFlush();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
		} catch (Exception e) {
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServiceResponse listDataSources() {
		LOGGER.info("Calling APi");
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			List<CustomerHistory> customers = CustomerDataSources.findAll().list();
			responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(), Status.SUCCESS.getResponseCode(),
					Status.SUCCESS.getResponseMessage(), Status.SUCCESS.getResponseDescription());
			response.put("customResponse", responseStatus);
			response.put("totalSize", customers.size());
			response.put("AllData", customers);

		} catch (Exception e) {
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		LOGGER.info(serviceResponse.toString());
		return serviceResponse;
	}

	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse updateSource(Long id, CustomerDataSources customerDataSources) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			CustomerDataSources customerDataSources2 = CustomerDataSources.findById(id);
			if (customerDataSources2 != null) {
				customerDataSources2.setColorName(customerDataSources.getColorName());
				customerDataSources2.setSourceName(customerDataSources.getSourceName());
				customerDataSources2.setUpdatedDate(LocalDateTime.now());
				customerDataSources2.persistAndFlush();
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

	@Transactional(value = TxType.REQUIRES_NEW)
	@Override
	public ServiceResponse deleteSource(Long id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		LinkedHashMap<Object, Object> response = new LinkedHashMap<>();
		CustomReponseStatus responseStatus = null;
		try {
			if (id > 0) {
				CustomerDataSources customerDataSources = CustomerDataSources.findById(id);
				customerDataSources.delete();
				responseStatus = new CustomReponseStatus(Status.SUCCESS.getResponseId(),
						Status.SUCCESS.getResponseCode(), Status.SUCCESS.getResponseMessage(),
						Status.SUCCESS.getResponseDescription());
				response.put("customResponse", responseStatus);
			} else {
				responseStatus = new CustomReponseStatus(Status.NOTEXIST.getResponseId(),
						Status.NOTEXIST.getResponseCode(), Status.NOTEXIST.getResponseMessage(),
						Status.NOTEXIST.getResponseDescription());
				response.put("customResponse", responseStatus);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseStatus = new CustomReponseStatus(Status.FAILED.getResponseId(), Status.FAILED.getResponseCode(),
					Status.FAILED.getResponseMessage(), Status.FAILED.getResponseDescription());
			response.put("customResponse", responseStatus);
		}
		serviceResponse.setServiceResponse(response);
		return serviceResponse;
	}

}
