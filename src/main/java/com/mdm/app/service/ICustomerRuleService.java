/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerRules;

/**
 * This interface ICustomerMasterService is used customer_rules Table
 * 
 * @author vickyrajput
 */
public interface ICustomerRuleService {

	/**
	 * Display all Customer rules
	 * 
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse listCustomerRule();

	/**
	 * Get Customer rule by id
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerRuleById(Long id);

	/**
	 * Create new Rule
	 * 
	 * @param customer {@link CustomerRules}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse createCustomerRule(CustomerRules customer);

	/**
	 * Update Customer Rule
	 * 
	 * @param id       {@link Long}
	 * @param customer {@link CustomerRules}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse updateCustomerRule(Long id, CustomerRules customer);

	public ServiceResponse deleteCustomerRule(Long id);

}
