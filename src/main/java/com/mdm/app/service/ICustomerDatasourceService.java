/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerDataSources;

/**
 * This interface ICustomerDatasourceService is used customer_datasource Table
 * 
 * @author vickyrajput
 */
public interface ICustomerDatasourceService {

	/**
	 * Display All Customer DataSources.
	 * 
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse listDataSources();

	/**
	 * Create Customer DataSources
	 * 
	 * @param customerDataSources {@link CustomerDataSources}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse createCustomerDataSource(CustomerDataSources customerDataSources);

	/**
	 * Update Customer Source
	 * 
	 * @param id       {@link Long}
	 * @param customerDataSources {@link CustomerDataSources}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse updateSource(Long id, CustomerDataSources customerDataSources);

	/**
	 * Delete Customer Source
	 * 
	 * @param id       {@link Long}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse deleteSource(Long id);

}
