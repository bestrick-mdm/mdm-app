/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service;

import com.mdm.app.dto.CustomerIDMapperDTO;
import com.mdm.app.dto.ServiceResponse;

/**
 * This interface ICustomerIdMapperService is used customer_Ids Table
 * 
 * @author vickyrajput
 *
 */
public interface ICustomerIdMapperService {

	/**
	 * Map Customer Data Via JBPM Process.
	 * 
	 * @param customer {@link CustomerIDMapperDTO}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse createCustomerIdMapper(CustomerIDMapperDTO customer);

}
