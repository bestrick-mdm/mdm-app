/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service;

import com.mdm.app.dto.CustomerDataReport;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerMaster;

/**
 * This interface ICustomerMasterService is used customer_master Table
 * 
 * @author vickyrajput
 */
public interface ICustomerMasterService {

	/**
	 * Display All Customer Master Data
	 * 
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse listCustomer();

	/**
	 * Display record by customerId
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerById(String customerId);

	/**
	 * Create Customer Master (Not used currently)
	 * 
	 * @param customer {@link CustomerMaster}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse createCustomer(CustomerMaster customer);

	/**
	 * update Customer Master (Not used currently because update customer data by
	 * rules.)
	 * 
	 * @param id       {@link Long}
	 * @param customer {@link CustomerMaster}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse updateCustomer(Long id, CustomerMaster customer);

	public ServiceResponse deleteCustomer(Long id);

	/**
	 * Search Customer Master By name or address
	 * 
	 * @param parameter {@link String}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerByNameOrAddress(String parameter);

	/**
	 * Generate Customer Data Report By tablename and fieldname.
	 * 
	 * @param tablename {@link String}
	 * @param fieldname {@link String}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerDataReport(String tablename, String fieldname);

	/**
	 * Get Customer Data Report in Cache infinispan By tablename and fieldname .
	 * 
	 * @param tablename {@link String}
	 * @param fieldname {@link String}
	 * @return {@link ServiceResponse}
	 */
	public CustomerDataReport getCustomerDataReportCache(String tablename, String fieldname);

	public ServiceResponse getAllColumnByTableName(String tableName);
	
	/**
	 * Create Customer Master
	 * 
	 * @param customerId {@link String}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse createCustomerByProcess(Long customerTableId);

}
