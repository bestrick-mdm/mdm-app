/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service;

import com.mdm.app.dto.ServiceResponse;

/**
 * This interface ICustomerMasterHistoryService is used customer_master_history
 * Table
 * 
 * @author vickyrajput
 *
 */
public interface ICustomerMasterHistoryService {

	/**
	 * Display all customer master history
	 * 
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse listCustomer();

	/**
	 * Get All Customer Master History with id
	 * 
	 * @param id {@link String}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerById(String id);

}
