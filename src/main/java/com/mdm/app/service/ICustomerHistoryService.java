/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.service;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerData;

/**
 * This interface ICustomerHistoryService is used customer_history Table
 * 
 * @author vickyrajput
 *
 */
public interface ICustomerHistoryService {

	/**
	 * Display All Customer history Data.
	 * 
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse listCustomerHistory();

	/**
	 * Get Customer History by Id
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerHistoryById(Long id);

	/**
	 * Create Customer records in History table.
	 * 
	 * @param customer {@link CustomerData}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse createCustomerHistory(CustomerData customer);

	/**
	 * Process Customer History via Jbpm Process
	 * 
	 * @param customerHistoryId {@link Long}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse processCustomerHistory(Long customerHistoryId);
	
	/**
	 * Get Customer History by customer Id
	 * 
	 * @param id {@link String}
	 * @return {@link ServiceResponse}
	 */
	public ServiceResponse getCustomerHistoryById(String id);
}
