/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.process.rule;

import java.io.Serializable;

import com.mdm.app.entity.CustomerData;
import com.mdm.app.entity.CustomerMaster;


/**
 * This class ProcessCustomerRulePojo is used by {@link ProcessCustomerRule} for
 * processing rule.
 * 
 * @author vickyrajput
 */
public class ProcessCustomerRulePojo implements Serializable {

	private static final long serialVersionUID = 1L;
	private CustomerData customerData;
	private CustomerMaster customerMaster;

	public CustomerData getCustomerData() {
		return customerData;
	}

	public void setCustomerData(CustomerData customerData) {
		this.customerData = customerData;
	}

	public CustomerMaster getCustomerMaster() {
		return customerMaster;
	}

	public void setCustomerMaster(CustomerMaster customerMaster) {
		this.customerMaster = customerMaster;
	}

}
