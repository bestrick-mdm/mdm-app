/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.process.rule;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.CustomerRule;
import com.mdm.app.entity.AddressMaster;
import com.mdm.app.entity.CustomerData;
import com.mdm.app.entity.CustomerMaster;
import com.mdm.app.entity.CustomerMasterHistory;
import com.mdm.app.entity.CustomerRules;
import com.mdm.app.entity.PhoneMaster;

import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import io.quarkus.panache.common.Sort.Direction;
import io.quarkus.vertx.ConsumeEvent;

/**
 * This class ProcessCustomerRule is used for process all Rule on customer data
 * and insert or Update CustomerMaster & Customer Master History
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class ProcessCustomerRule {
	private final static Logger LOGGER = Logger.getLogger(ProcessCustomerRule.class.getName());
	private final static String address = "address";
	private final static String phone = "phone";
	@Inject
	EntityManager em;

	@Inject
	ErrorBean errorBean;  
	/**
	 * This method is EventBased and process All Rules using {@link CustomerRules}.
	 * 
	 * @param customerRulePojo {@link ProcessCustomerRulePojo}
	 */
	@SuppressWarnings("unused")
	@ConsumeEvent(value = "customer-rule", blocking = true)
	@Transactional
	public void saveCustomer(ProcessCustomerRulePojo customerRulePojo) {
		LOGGER.info("Coming processing Rule !!");
		List<CustomerRules> rulesList = CustomerRules
				.find("fieldName not like :fieldName", Parameters.with("fieldName", "SOURCE_%")).list();
		LOGGER.info("Coming processing Rule !!" + rulesList.toString());
		try {
			for (CustomerRules customerRules : rulesList) {
				LOGGER.info("Processing :- " + customerRules.toString());
				if (StringUtils.equalsIgnoreCase(address, customerRules.getFieldName())) {
					applyRuleAddress(customerRulePojo.getCustomerData(), customerRulePojo.getCustomerMaster(),
							customerRules);
				} else if (StringUtils.equalsIgnoreCase(phone, customerRules.getFieldName())) {
					applyRulePhone(customerRulePojo.getCustomerData(), customerRulePojo.getCustomerMaster(),
							customerRules);
				} else if (StringUtils.equalsIgnoreCase(CustomerRule.RECENCY.name(), customerRules.getRuleName())) {
					String newValue = BeanUtils.getProperty(customerRulePojo.getCustomerData().getCustomer(),
							customerRules.getFieldName());
					LOGGER.info(" if Coming processing Rule !!" + newValue);
					if (StringUtils.isNotBlank(newValue)) {
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(), customerRules.getFieldName(),
								newValue);
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Rule", customerRules.getRuleName());
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Source",
								customerRulePojo.getCustomerData().getCustomer().getFromSource());
					}
				} else if (StringUtils.equalsIgnoreCase(CustomerRule.AGGREGATE.name(), customerRules.getRuleName())) {
					String newValue = BeanUtils.getProperty(customerRulePojo.getCustomerData().getCustomer(),
							customerRules.getFieldName());
					String oldValue = BeanUtils.getProperty(customerRulePojo.getCustomerMaster(),
							customerRules.getFieldName());
					LOGGER.info(oldValue + " Coming Else processing Rule !!" + newValue);
					if (StringUtils.isNotBlank(newValue)) {
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(), customerRules.getFieldName(),
								oldValue + "," + newValue);
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Rule", customerRules.getRuleName());
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Source",
								customerRulePojo.getCustomerData().getCustomer().getFromSource());
					}
				} else if (StringUtils.equalsIgnoreCase(CustomerRule.PRIORITY.name(), customerRules.getRuleName())
						&& StringUtils.equalsIgnoreCase(
								"SOURCE_" + customerRulePojo.getCustomerData().getCustomer().getFromSource(),
								customerRules.getFromSource())) {
					String source = BeanUtils.getProperty(customerRulePojo.getCustomerMaster(),
							customerRules.getFieldName() + "Source");
					source = source == null ? customerRulePojo.getCustomerMaster().getFromSource() : source;
					LOGGER.info("Source " + source);
					Map<String, Object> params = new LinkedHashMap<>();
					params.put("fieldName", customerRules.getFieldName());
					params.put("fromSource", "SOURCE_" + source);
					LOGGER.info("Param Map " + params);
					CustomerRules rule = CustomerRules
							.find("fieldName = :fieldName and fromSource = :fromSource ", params).singleResult();
					LOGGER.info("Param Map " + rule);
					if (customerRules.getPriority() >= rule.getPriority()) {
						String newValue = BeanUtils.getProperty(customerRulePojo.getCustomerData().getCustomer(),
								customerRules.getFieldName());
						LOGGER.info("Param newValue " + newValue);
						if (StringUtils.isNotBlank(newValue)) {
							BeanUtils.setProperty(customerRulePojo.getCustomerMaster(), customerRules.getFieldName(),
									newValue);
							BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
									customerRules.getFieldName() + "Rule", customerRules.getRuleName());
							BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
									customerRules.getFieldName() + "Source",
									customerRulePojo.getCustomerData().getCustomer().getFromSource());
						}
					}
				} else if (StringUtils.equalsIgnoreCase(CustomerRule.MIN.name(), customerRules.getRuleName())) {
					String newValue = BeanUtils.getProperty(customerRulePojo.getCustomerData().getCustomer(),
							customerRules.getFieldName());
					String oldValue = BeanUtils.getProperty(customerRulePojo.getCustomerMaster(),
							customerRules.getFieldName());
					LOGGER.info(oldValue + " Coming Min processing Rule !!" + newValue);
					if (StringUtils.isNotBlank(newValue)) {
						int minVal = Math.min(Integer.parseInt(newValue), Integer.parseInt(oldValue));
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(), customerRules.getFieldName(),
								minVal);
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Rule", customerRules.getRuleName());
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Source",
								customerRulePojo.getCustomerData().getCustomer().getFromSource());
					}
				} else if (StringUtils.equalsIgnoreCase(CustomerRule.MAX.name(), customerRules.getRuleName())) {
					String newValue = BeanUtils.getProperty(customerRulePojo.getCustomerData().getCustomer(),
							customerRules.getFieldName());
					String oldValue = BeanUtils.getProperty(customerRulePojo.getCustomerMaster(),
							customerRules.getFieldName());
					LOGGER.info(oldValue + " Coming MAX processing Rule !!" + newValue);
					if (StringUtils.isNotBlank(newValue)) {
						int maxVal = Math.max(Integer.parseInt(newValue), Integer.parseInt(oldValue));
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(), customerRules.getFieldName(),
								maxVal);
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Rule", customerRules.getRuleName());
						BeanUtils.setProperty(customerRulePojo.getCustomerMaster(),
								customerRules.getFieldName() + "Source",
								customerRulePojo.getCustomerData().getCustomer().getFromSource());
					}
				}
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "saveCustomer");			
			e.printStackTrace();
			
		}
		LOGGER.info("Customer Data Rule !! " + customerRulePojo.getCustomerData().getCustomer().toString());
		LOGGER.info("After Apply Processing Rule !! " + customerRulePojo.getCustomerMaster().toString());
		Session session = em.unwrap(Session.class);
		session.saveOrUpdate(customerRulePojo.getCustomerMaster());
		LOGGER.info("Update Data Successfully !!");
		CustomerMasterHistory masterHistory = CustomerMasterHistory
				.find("customerId = :customerId ", Sort.by("version", Direction.Descending),
						Parameters.with("customerId", customerRulePojo.getCustomerMaster().getCustomerId()))
				.firstResult();
		int version = masterHistory.getVersion();
		LOGGER.info("History :- " + masterHistory);
		LOGGER.info("Master :- " + customerRulePojo.getCustomerMaster().toString());
		masterHistory = new CustomerMasterHistory();
		ModelMapper mapper = new ModelMapper();
		mapper.map(customerRulePojo.getCustomerMaster(), masterHistory);
		masterHistory.setVersion(version + 1);
		LOGGER.info("Before insert History :- " + masterHistory.toString());
		masterHistory.persistAndFlush();
		LOGGER.info("Save Data History Successfully !!");
	}

	/**
	 * Process only address rule.
	 * 
	 * @param customerData   {@link CustomerData}
	 * @param customerMaster {@link CustomerMaster}
	 * @param customerRules  {@link CustomerRules}
	 */
	public void applyRuleAddress(CustomerData customerData, CustomerMaster customerMaster,
			CustomerRules customerRules) {

		try {
			LOGGER.info("Processing :- " + customerRules.toString());
			if (StringUtils.equalsIgnoreCase(CustomerRule.RECENCY.name(), customerRules.getRuleName())) {
				long deladdress = AddressMaster.delete("customermaster_id =:customerMaster",
						Parameters.with("customerMaster", customerMaster.id));
				LOGGER.info("Delete Address count :- " + deladdress);
				ModelMapper mapper = new ModelMapper();
				List<AddressMaster> addressMasters = mapper.map(customerData.getCustomer().getAddress(),
						new TypeToken<List<AddressMaster>>() {
						}.getType());
				customerMaster.setAddress(addressMasters);
				customerMaster.setAddressRule(customerRules.getRuleName());
				customerMaster.setAddressSource(customerData.getCustomer().getFromSource());
			} else if (StringUtils.equalsIgnoreCase(CustomerRule.AGGREGATE.name(), customerRules.getRuleName())) {
				ModelMapper mapper = new ModelMapper();
				List<AddressMaster> addressMasters = mapper.map(customerData.getCustomer().getAddress(),
						new TypeToken<List<AddressMaster>>() {
						}.getType());
				customerMaster.getAddress().addAll(addressMasters);
				customerMaster.setAddressRule(customerRules.getRuleName());
				customerMaster.setAddressSource(customerData.getCustomer().getFromSource());
			} else if (StringUtils.equalsIgnoreCase(CustomerRule.PRIORITY.name(), customerRules.getRuleName())
					&& StringUtils.equalsIgnoreCase("SOURCE_" + customerData.getCustomer().getFromSource(),
							customerRules.getFromSource())) {
				String source = customerMaster.getAddressSource();
				LOGGER.info("Source " + source);
				Map<String, Object> params = new LinkedHashMap<>();
				params.put("fieldName", customerRules.getFieldName());
				params.put("fromSource", "SOURCE_" + source);
				LOGGER.info("Param Map " + params);
				CustomerRules rule = CustomerRules.find("fieldName = :fieldName and fromSource = :fromSource ", params)
						.singleResult();
				LOGGER.info("Param Map " + rule);
				if (customerRules.getPriority() >= rule.getPriority()) {
					long deladdress = AddressMaster.delete("customermaster_id =:customerMaster",
							Parameters.with("customerMaster", customerMaster.id));
					LOGGER.info("Delete Address count :- " + deladdress);
					customerMaster.getAddress().clear();
					ModelMapper mapper = new ModelMapper();
					List<AddressMaster> addressMasters = mapper.map(customerData.getCustomer().getAddress(),
							new TypeToken<List<AddressMaster>>() {
							}.getType());
					customerMaster.setAddress(addressMasters);
					customerMaster.setAddressRule(customerRules.getRuleName());
					customerMaster.setAddressSource(customerData.getCustomer().getFromSource());
				}
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "applyRuleAddress");
			e.printStackTrace();
		}

	}

	/**
	 * Process only phone rule.
	 * 
	 * @param customerData   {@link CustomerData}
	 * @param customerMaster {@link CustomerMaster}
	 * @param customerRules  {@link CustomerRules}
	 */
	public void applyRulePhone(CustomerData customerData, CustomerMaster customerMaster, CustomerRules customerRules) {
		try {
			LOGGER.info("Processing :- " + customerRules.toString());
			if (StringUtils.equalsIgnoreCase(CustomerRule.RECENCY.name(), customerRules.getRuleName())) {
				long deladdress = PhoneMaster.delete("customermaster_id =:customerMaster",
						Parameters.with("customerMaster", customerMaster.id));
				LOGGER.info("Delete Address count :- " + deladdress);
				ModelMapper mapper = new ModelMapper();
				List<PhoneMaster> phoneMasters = mapper.map(customerData.getCustomer().getPhone(),
						new TypeToken<List<PhoneMaster>>() {
						}.getType());
				customerMaster.setPhone(phoneMasters);
				customerMaster.setPhoneRule(customerRules.getRuleName());
				customerMaster.setPhoneSource(customerData.getCustomer().getFromSource());
			} else if (StringUtils.equalsIgnoreCase(CustomerRule.AGGREGATE.name(), customerRules.getRuleName())) {
				ModelMapper mapper = new ModelMapper();
				List<PhoneMaster> phoneMasters = mapper.map(customerData.getCustomer().getPhone(),
						new TypeToken<List<PhoneMaster>>() {
						}.getType());
				customerMaster.getPhone().addAll(phoneMasters);
				customerMaster.setPhoneRule(customerRules.getRuleName());
				customerMaster.setPhoneSource(customerData.getCustomer().getFromSource());
			} else if (StringUtils.equalsIgnoreCase(CustomerRule.PRIORITY.name(), customerRules.getRuleName())
					&& StringUtils.equalsIgnoreCase("SOURCE_" + customerData.getCustomer().getFromSource(),
							customerRules.getFromSource())) {
				String source = customerMaster.getPhoneSource();
				LOGGER.info("Source " + source);
				Map<String, Object> params = new LinkedHashMap<>();
				params.put("fieldName", customerRules.getFieldName());
				params.put("fromSource", "SOURCE_" + source);
				LOGGER.info("Param Map " + params);
				CustomerRules rule = CustomerRules.find("fieldName = :fieldName and fromSource = :fromSource ", params)
						.singleResult();
				LOGGER.info("Param Map " + rule);
				if (customerRules.getPriority() >= rule.getPriority()) {
					long deladdress = PhoneMaster.delete("customermaster_id =:customerMaster",
							Parameters.with("customerMaster", customerMaster.id));
					LOGGER.info("Delete Address count :- " + deladdress);
					ModelMapper mapper = new ModelMapper();
					List<PhoneMaster> phoneMasters = mapper.map(customerData.getCustomer().getPhone(),
							new TypeToken<List<PhoneMaster>>() {
							}.getType());
					customerMaster.setPhone(phoneMasters);
					customerMaster.setPhoneRule(customerRules.getRuleName());
					customerMaster.setPhoneSource(customerData.getCustomer().getFromSource());
				}
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "applyRulePhone");
			e.printStackTrace();
		}
	}

}
