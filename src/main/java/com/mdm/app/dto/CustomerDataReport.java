/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CustomerDataReport {

	int nullCount;
	int uniqueCount;
	int nonUniqueCount;
	int totalRecords;
	Float nullPer;
	Float uniquePer;
	Float nonUniquePer;
	BigInteger decimal;
	BigInteger string;
	BigInteger integer;
	BigInteger date;
	List<CustomerValueDTO> value = new ArrayList<>();

}
