/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.util.LinkedHashMap;
import java.util.Map;

public class Response {

	Map<String, Object> response = new LinkedHashMap<>();

	public Map<String, Object> getResponse() {
		return response;
	}

	public void setResponse(Map<String, Object> response) {
		this.response = response;
	}

}
