/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhoneDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	String countryCode;
	String extension;
	String phoneNumber;
	String phoneType;
	String preferred;
}
