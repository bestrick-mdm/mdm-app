/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerWithRuleAndSourceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	String customerId;
	String firstName;
	String firstNameRule;
	String firstNameSource;
	String lastName;
	String lastNameRule;
	String lastNameSource;
	String suffix;
	String suffixRule;
	String suffixSource;
	String title;
	String titleRule;
	String titleSource;
	String gender;
	String multiChanelCommunicationContext;
	String employment;
	String birthState;
	String birthCountry;
	String maritualStatus;
	String incomeBand;
	String ageBand;
	String hasChildren;
	String internalNotes;
	String email;
	String fromSource;

	List<AddressDTO> address;

	List<PhoneDTO> phone;
}
