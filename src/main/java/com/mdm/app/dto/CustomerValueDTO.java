/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.math.BigInteger;

public class CustomerValueDTO {
	String value;
	BigInteger frequency;
	Integer length;
	float percentage;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public BigInteger getFrequency() {
		return frequency;
	}

	public void setFrequency(BigInteger frequency) {
		this.frequency = frequency;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

}
