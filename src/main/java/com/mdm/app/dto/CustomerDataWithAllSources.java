/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mdm.app.entity.CustomerMaster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDataWithAllSources implements Serializable{

	private static final long serialVersionUID = 1L;
	CustomerMaster customerMasterDTO;
	List<CustomerDTO> customerHistoryDTOs = new ArrayList<CustomerDTO>();
}
