/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class AddressDTO is return address information.
 * 
 * @author vickyrajput
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	String street_1;
	String street_2;
	String aptSuite;
	String city;
	String state;
	String zip;
	String country;
	String preferred;
}
