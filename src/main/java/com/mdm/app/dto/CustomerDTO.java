/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.mdm.app.entity.HomeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	String customerId;
	String firstName;
	String lastName;
	String suffix;
	String title;
	String gender;
	String multiChanelCommunicationContext;
	String employment;
	String birthState;
	String birthCountry;
	String maritualStatus;
	String incomeBand;
	String ageBand;
	String hasChildren;
	String internalNotes;
	String email;
	String fromSource;
	String fromSourceColor;
// Start Addtitional fields MDM-14
	Integer age;
	String maritialStatus;
	String education;
	String grade;
	String empTitle;
	String empLength;
	HomeType homeOwnership;
	Long zipCode;
	Integer familySize;
	Long ssn;
	Long sumCertificateOfDeposit;
	Long sumCreditCard;
	Long sumDebitCard;
	Long sumInternatinalDebit;
	Long sumSavingsAccount;
	Long sumCheckingAmount;
	Long sumVehicleLoan;
	Long sumPersonalLoan;
	Long sumInsurance;
	Long sumSecurities;
	Long sumOverDraft;
// End  MDM-14

	List<AddressDTO> address;
	List<PhoneDTO> phone;

	LocalDateTime createdDate;

}
