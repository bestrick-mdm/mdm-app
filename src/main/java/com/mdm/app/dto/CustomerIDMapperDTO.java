/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.dto;

public class CustomerIDMapperDTO {

	String customerId;
	String mapCustomerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMapCustomerId() {
		return mapCustomerId;
	}

	public void setMapCustomerId(String mapCustomerId) {
		this.mapCustomerId = mapCustomerId;
	}

}
