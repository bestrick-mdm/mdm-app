/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mdm.app.dto.CustomerIDMapperDTO;
import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.service.ICustomerIdMapperService;

/**
 * This api class CustomerIdsMapperResource is used by Jbpm to mapped customer
 * NON-CONSISTENT with CONSISTENT Customer
 * 
 * @author vickyrajput
 */
@Path("/customer/mapper")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerIdsMapperResource {

	@Inject
	ICustomerIdMapperService customerIdMapperService;

	/**
	 * Map Customer Data
	 * 
	 * @param customerIDMapperDTO {@link CustomerIDMapperDTO}
	 * @return {@link ServiceResponse}
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse createCustomer(CustomerIDMapperDTO customerIDMapperDTO) {
		return customerIdMapperService.createCustomerIdMapper(customerIDMapperDTO);
	}
}
