/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerRules;
import com.mdm.app.service.ICustomerRuleService;

/**
 * Api for Customer Rule.
 * 
 * @author vickyrajput
 */
@Path("/customer/rule")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerRuleResource {

	@Inject
	ICustomerRuleService customerRuleService;

	/**
	 * Display All Customer Rules.
	 * 
	 * @return {@link ServiceResponse}
	 */
	@GET
	public ServiceResponse listCustomerRule() {
		return customerRuleService.listCustomerRule();
	}

	/**
	 * Get Customer Rule By id.
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/{id}")
	public ServiceResponse getCustomerRuleById(@PathParam("id") Long id) {
		return customerRuleService.getCustomerRuleById(id);
	}

	/**
	 * Create new Customer Rule for field.
	 * 
	 * @param customerRules {@link CustomerRules}
	 * @return {@link ServiceResponse}
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse createCustomerRule(CustomerRules customerRules) {
		return customerRuleService.createCustomerRule(customerRules);
	}

	/**
	 * Update Customer Rule for particular id
	 * 
	 * @param id            {@link Long}
	 * @param customerRules {@link CustomerRules}
	 * @return {@link ServiceResponse}
	 */
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse updateCustomerRule(@PathParam("id") Long id, CustomerRules customerRules) {
		return customerRuleService.updateCustomerRule(id, customerRules);
	}

	/**
	 * Delete Customer Rule
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse deleteCustomerRule(@PathParam("id") Long id) {
		return customerRuleService.deleteCustomerRule(id);
	}
}