/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerData;
import com.mdm.app.service.ICustomerHistoryService;

/**
 * This class CustomerHistoryResource is used for process and insert new customer data
 * 
 * @author vickyrajput
 */
@Path("/customer/history")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerHistoryResource {

	@Inject
	ICustomerHistoryService customerService;

	/**
	 * Display All Customer History Data coming via Api or Queue
	 */
	@GET
	public ServiceResponse listCustomer() {
		return customerService.listCustomerHistory();
	}

	/**
	 * Search Customer History Data through Id coming via Api or Queue
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
//	@GET
//	@Path("/{id}")
//	public ServiceResponse getCustomerById(@PathParam("id") Long id) {
//		return customerService.getCustomerHistoryById(id);
//	}

	/**
	 * Create Customer History Data coming via Api or Queue
	 * 
	 * @param customerData {@link CustomerData}
	 * @return {@link ServiceResponse}
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse createCustomer(CustomerData customerData) {
		return customerService.createCustomerHistory(customerData);
	}

	/**
	 * Process Customer History Data coming after Jbpm Process Complete
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/process/rule/{id}")
	public ServiceResponse processCustomerRuleAgain(@PathParam("id") Long id) {
		return customerService.processCustomerHistory(id);
	}
	
	/**
	 * Search Customer History Data through Id coming via Api or Queue
	 * 
	 * @param id {@link String}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/{id}")
	public ServiceResponse getCustomerById(@PathParam("id") String id) {
		return customerService.getCustomerHistoryById(id);
	}
}