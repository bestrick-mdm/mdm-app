/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.service.ICustomerMasterHistoryService;

/**
 * Api for Customer master history.
 * 
 * @author vickyrajput
 */
@Path("/customer/history/sources")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerMasterHistoryResource {

	@Inject
	ICustomerMasterHistoryService customerMasterHistoryService;

	/**
	 * Display all Customer master history.
	 * 
	 * @return {@link ServiceResponse}
	 */
	@GET
	public ServiceResponse listCustomer() {
		return customerMasterHistoryService.listCustomer();
	}

	/**
	 * Display Customer Master History by Id.
	 * 
	 * @param id {@link String}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/{id}")
	public ServiceResponse getCustomerById(@PathParam("id") String id) {
		return customerMasterHistoryService.getCustomerById(id);
	}

}