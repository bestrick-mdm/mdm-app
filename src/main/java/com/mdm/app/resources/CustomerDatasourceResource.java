package com.mdm.app.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerData;
import com.mdm.app.entity.CustomerDataSources;
import com.mdm.app.service.ICustomerDatasourceService;

/**
 * This class CustomerDatasourceResource is used for insert new customer datasource
 * 
 * @author vickyrajput
 */
@Path("/customer/datasources")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerDatasourceResource {
	
	@Inject
	ICustomerDatasourceService customerDatasourceService;
	
	/**
	 * Display All Customer History Data coming via Api or Queue
	 */
	@GET
	public ServiceResponse listDataSources() {
		return customerDatasourceService.listDataSources();
	}
	/**
	 * Create Customer Datasources
	 * 
	 * @param customerData {@link CustomerData}
	 * @return {@link ServiceResponse}
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse createCustomer(CustomerDataSources customerDataSources) {
		return customerDatasourceService.createCustomerDataSource(customerDataSources);
	}
	
	/**
	 * Update Customer Datasources for particular id
	 * 
	 * @param id            {@link Long}
	 * @param customerDataSources {@link CustomerDataSources}
	 * @return {@link ServiceResponse}
	 */
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse updateSource(@PathParam("id") Long id, CustomerDataSources customerDataSources) {
		return customerDatasourceService.updateSource(id, customerDataSources);
	}

	/**
	 * Delete Customer Datasources
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse deleteSource(@PathParam("id") Long id) {
		return customerDatasourceService.deleteSource(id);
	}
}
