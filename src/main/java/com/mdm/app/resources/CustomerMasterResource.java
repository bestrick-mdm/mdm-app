/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mdm.app.dto.ServiceResponse;
import com.mdm.app.entity.CustomerMaster;
import com.mdm.app.service.ICustomerMasterService;

/**
 * Api for Customer Master.
 * 
 * @author vickyrajput
 */
@Path("/customer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerMasterResource {

	@Inject
	ICustomerMasterService customerService;

	/**
	 * Display all Customer master data.
	 * 
	 * @return {@link ServiceResponse}
	 */
	@GET
	public ServiceResponse listCustomer() {
		return customerService.listCustomer();
	}

	/**
	 * Get Customer Master By Id.
	 * 
	 * @param id {@link Long}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/{id}")
	public ServiceResponse getCustomerById(@PathParam("id") String customerId) {
		return customerService.getCustomerById(customerId);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse createCustomer(CustomerMaster customer) {
		return customerService.createCustomer(customer);
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse updateCustomer(@PathParam("id") Long id, CustomerMaster customer) {
		return customerService.updateCustomer(id, customer);
	}

	/**
	 * Search Customer By Name or Address.
	 * 
	 * @param nameOrAddress {@link String}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/searchByNameOrAddress/{nameOrAddress}")
	public ServiceResponse getCustomerByNameOrAddress(@PathParam("nameOrAddress") String nameOrAddress) {
		return customerService.getCustomerByNameOrAddress(nameOrAddress);
	}

	/**
	 * Get Data Report for particular tablename with fieldname
	 * 
	 * @param tablename {@link String}
	 * @param fieldname {@link String}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/getDataReport/{tablename}/{fieldname}")
	public ServiceResponse getCustomerDataReport(@PathParam("tablename") String tablename,
			@PathParam("fieldname") String fieldname) {
		return customerService.getCustomerDataReport(tablename, fieldname);
	}

	/**
	 * Search Customer By Name or Address.
	 * 
	 * @param nameOrAddress {@link String}
	 * @return {@link ServiceResponse}
	 */
	@GET
	@Path("/getColumns/{tableName}")
	public ServiceResponse getAllColumnByTableName(@PathParam("tableName") String tableName) {
		return customerService.getAllColumnByTableName(tableName);
	}

	@GET
	@Path("/createCustomerByProcess/{customerTableId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public ServiceResponse createCustomerByProcess(@PathParam("customerTableId") Long customerTableId) {
		return customerService.createCustomerByProcess(customerTableId);
	}
}