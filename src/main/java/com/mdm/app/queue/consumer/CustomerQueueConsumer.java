/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.queue.consumer;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.glassfish.jersey.client.ClientConfig;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.mdm.app.common.ErrorBean;
import com.mdm.app.constant.CustomerRule;
import com.mdm.app.entity.CustomerData;
import com.mdm.app.entity.CustomerIdMapper;
import com.mdm.app.entity.CustomerMaster;
import com.mdm.app.entity.CustomerMasterHistory;
import com.mdm.app.entity.CustomerRules;
import com.mdm.app.jbpm.model.CustomerDataModel;
import com.mdm.app.process.rule.ProcessCustomerRulePojo;

import io.quarkus.panache.common.Parameters;
import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.configuration.ProfileManager;
import io.quarkus.vertx.ConsumeEvent;
import io.vertx.axle.core.eventbus.EventBus;

/**
 * This class CustomerQueueConsumer.
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class CustomerQueueConsumer {
	private final static Logger LOGGER = Logger.getLogger(CustomerQueueConsumer.class.getName());

	@ConfigProperty(name = "mdm.jbpm.url")
	String host;

	@ConfigProperty(name = "mdm.jbpm.path", defaultValue = "/customer/process/start")
	String path;

	@Inject
	EventBus bus;

	@Inject
	EntityManager em;

	@Inject
	ErrorBean errorBean;

	void onStart(@Observes StartupEvent ev) {
		LOGGER.info("The application is starting with profile " + ProfileManager.getActiveProfile());
	}

	/**
	 * This method is used for kafka queue and queue configured name is "customer".
	 * 
	 * @param message {@link String}
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 */
	@Incoming("customer")
	public void onMessage(String message) throws JsonMappingException, JsonProcessingException {
		LOGGER.info(message);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		CustomerData customerEntity = mapper.readValue(message, CustomerData.class);
		bus.publish("customer-message", customerEntity);
	}

	/**
	 * This is called by event and event name "customer-message".
	 * 
	 * @param customerData {@link CustomerData}
	 */
	@ConsumeEvent(value = "customer-message", blocking = true)
	@Transactional
	public void processCustomerData(CustomerData customerData) {
		LOGGER.info("Consumed Customer !!");
		customerData.getCustomer().persistAndFlush();

		LOGGER.info("Insert successfully !");
		startProcessingCustomerData(customerData);
	}

	@SuppressWarnings("unused")
	@Transactional
	public void startProcessingCustomerData(CustomerData customerData) {
		try {
			boolean jbpmFlag = Boolean.TRUE;
			List<String> customerIds = new ArrayList<String>();
			CustomerRules rules = CustomerRules
					.find("fieldName = :fieldName ",
							Parameters.with("fieldName", "SOURCE_" + customerData.getCustomer().getFromSource()))
					.firstResult();
			CustomerMaster customerMaster = null;
			LOGGER.info("fetch  ! " + rules);
			if (CustomerRule.CONSISTENT.name().equals(rules.getRuleName())) {
				LOGGER.info("Coming CONSISTENT rules ! " + jbpmFlag);
				jbpmFlag = Boolean.FALSE;
				customerMaster = CustomerMaster
						.find("customerId = :customerId ",
								Parameters.with("customerId", customerData.getCustomer().getCustomerId()))
						.firstResult();
				if (customerMaster == null) {
					CustomerIdMapper customerIdMapper = CustomerIdMapper
							.find("primaryCustomerId = :primaryCustomerId ",
									Parameters.with("primaryCustomerId", customerData.getCustomer().getCustomerId()))
							.firstResult();
					LOGGER.info("Coming Consistent Mapping customer id ! " + customerIdMapper);
					if (customerIdMapper != null) {						
						customerData.getCustomer().setCustomerId(customerIdMapper.getSecondaryCustomerId());
						customerMaster = CustomerMaster
								.find("customerId = :customerId ",
										Parameters.with("customerId", customerData.getCustomer().getCustomerId()))
								.firstResult();
					}
				}
			} else {
				CustomerIdMapper customerIdMapper = CustomerIdMapper
						.find("primaryCustomerId = :primaryCustomerId ",
								Parameters.with("primaryCustomerId", customerData.getCustomer().getCustomerId()))
						.firstResult();
				LOGGER.info("Coming Mapping customer id ! " + customerIdMapper);
				if (customerIdMapper != null) {
					jbpmFlag = Boolean.FALSE;
					customerData.getCustomer().setCustomerId(customerIdMapper.getSecondaryCustomerId());
					customerMaster = CustomerMaster
							.find("customerId = :customerId ",
									Parameters.with("customerId", customerData.getCustomer().getCustomerId()))
							.firstResult();
				} else {
					List<CustomerRules> rulesList = CustomerRules.find(
							"matchingWeight <> 0.0 and (fromSource is null or fromSource = :fromSource ) ",
							Parameters.with("fromSource", "SOURCE_" + customerData.getCustomer().getFromSource()))
							.list();
					SearchSession searchSession = Search.session(em);
					LOGGER.info("-->" + rulesList.toString());
					SearchResult<CustomerMaster> result = searchSession.search(CustomerMaster.class)
							.where(f -> f.bool(b -> {
								rulesList.forEach(rule -> {
									String fieldValue = "";
									try {
										fieldValue = BeanUtils.getProperty(customerData.getCustomer(),
												rule.getFieldName());
									} catch (IllegalAccessException | InvocationTargetException
											| NoSuchMethodException e) {
										LOGGER.info("Error coming -->");
										e.printStackTrace();
									}
									b.should(f.match().field(rule.getFieldName()).matching(fieldValue)
											.boost(rule.getMatchingWeight()).fuzzy());
								});
							})).sort(f -> f.score()).fetch(5);

					List<Float> hits = searchSession.search(CustomerMaster.class).select(f -> f.score())
							.where(f -> f.bool(b -> {
								rulesList.forEach(rule -> {
									String fieldValue = "";
									try {
										fieldValue = BeanUtils.getProperty(customerData.getCustomer(),
												rule.getFieldName());
									} catch (IllegalAccessException | InvocationTargetException
											| NoSuchMethodException e) {
										LOGGER.info("Error coming -->");
										e.printStackTrace();
									}
									b.should(f.match().field(rule.getFieldName()).matching(fieldValue)
											.boost(rule.getMatchingWeight()).fuzzy());
								});
							})).fetchHits(5);

					LOGGER.info("Score Value :- " + hits.toString());
					LOGGER.info("searchResult Value :- " + result.toString());
					if (result.hits().size() > 0 && hits.get(0) >= 2.5) {
						customerMaster = result.hits().get(0);
						jbpmFlag = Boolean.FALSE;
						customerIdMapper = new CustomerIdMapper();
						customerIdMapper.setPrimaryCustomerId(customerData.getCustomer().getCustomerId());
						customerIdMapper.setSecondaryCustomerId(customerMaster.getCustomerId());
						customerIdMapper.setSecondarySource(customerData.getCustomer().getFromSource());
						customerIdMapper.persistAndFlush();
						LOGGER.info("Save Customer Mapper Value ***** ");
					} else if (result.hits().size() == 0) {
						jbpmFlag = Boolean.FALSE;
					} else {
						result.hits().forEach(e -> customerIds.add(e.getCustomerId()));
					}
				}
			}
			if (jbpmFlag) {
				LOGGER.info("Start Jbpm process with input parameter! " + customerIds.toString());
				CustomerDataModel customerDataModel = new CustomerDataModel();
				customerDataModel.getCustomerIds().addAll(customerIds);
				customerDataModel.setCustomerId(customerData.getCustomer().getCustomerId());
				customerDataModel.setCustomerTableId(customerData.getCustomer().id);
				ClientConfig config = new ClientConfig();
				config.register(JacksonJsonProvider.class);
				Client client = ClientBuilder.newClient(config);
				WebTarget target = client.target(host).path(path);
				Response response = target.request(MediaType.APPLICATION_JSON).post(Entity.json(customerDataModel));
				LOGGER.info("Webclient respone :- " + response.toString());
				LOGGER.info("Start Jbpm process Successfully :: ");
			} else {
				LOGGER.info("Insert Master successfully ! " + customerMaster);
				if (customerMaster == null) {
					customerMaster = new CustomerMaster();
					ModelMapper mapper = new ModelMapper();
					mapper.map(customerData.getCustomer(), customerMaster);
					LOGGER.info("After Mapper successfully ! " + customerMaster);
					customerMaster.persistAndFlush();
					LOGGER.info("Insert Master successfully ! " + customerMaster);
					CustomerMasterHistory masterHistory = new CustomerMasterHistory();
					mapper.map(customerMaster, masterHistory);
					masterHistory.setVersion(1);
					masterHistory.persistAndFlush();
					LOGGER.info("Insert Master History successfully ! " + masterHistory);
					CustomerIdMapper customerIdMapper = new CustomerIdMapper();
					customerIdMapper.setPrimaryCustomerId(customerData.getCustomer().getCustomerId());
					customerIdMapper.setSecondaryCustomerId(customerData.getCustomer().getCustomerId());
					customerIdMapper.setSecondarySource(customerData.getCustomer().getFromSource());
					customerIdMapper.persistAndFlush();
					LOGGER.info("Create New Customer Map Customer Id successfully ! ");
				} else {
					ProcessCustomerRulePojo customerRulePojo = new ProcessCustomerRulePojo();
					customerRulePojo.setCustomerData(customerData);
					customerRulePojo.setCustomerMaster(customerMaster);
					bus.publish("customer-rule", customerRulePojo);
					LOGGER.info("Process Rule successfully !");
				}
			}
		} catch (Exception e) {
			errorBean.createError(e, this.getClass().getName(), "startProcessingCustomerData");
			e.printStackTrace();
		}
	}

}
