/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.constant;

/**
 * This Interface QueryConstant is Store DB.
 * 
 * @author vickyrajput
 */
public interface QueryConstant {

	String GROUP_BY_QUERY = "SELECT COUNT(CustomerID),LENGTH(Country), Country FROM Customers GROUP BY Country";
}
