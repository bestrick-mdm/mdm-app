/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.constant;

/**
 * The Enum CustomerRule Type.
 * 
 * @author vickyrajput
 */
public enum CustomerRule {

	RECENCY, AGGREGATE, PRIORITY, MIN, MAX, CONSISTENT
}
