/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.elastic.config;

import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurationContext;
import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurer;

/**
 * This class AnalysisConfigurer is used Elastic Search Configuration.
 * 
 * @author vickyrajput
 */
public class AnalysisConfigurer implements ElasticsearchAnalysisConfigurer {

	@Override
	public void configure(ElasticsearchAnalysisConfigurationContext context) {
		context.analyzer("name").custom().tokenizer("standard").tokenFilters("asciifolding", "lowercase");

		context.analyzer("email").custom().tokenizer("standard").tokenFilters("asciifolding", "lowercase");

		context.normalizer("sort").custom().tokenFilters("asciifolding", "lowercase");
	}

}
