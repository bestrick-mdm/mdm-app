/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.scheduler;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdm.app.common.ErrorBean;
import com.mdm.app.dto.CustomerDataReport;
import com.mdm.app.service.ICustomerMasterService;

import io.quarkus.infinispan.client.Remote;
import io.quarkus.scheduler.Scheduled;
import io.quarkus.scheduler.ScheduledExecution;

/**
 * This class DataReportScheduler.
 * 
 * @author vickyrajput
 */
@ApplicationScoped
public class DataReportScheduler {
	private final static Logger LOGGER = Logger.getLogger(DataReportScheduler.class.getName());
	@Inject
	EntityManager em;

	@Inject
	ICustomerMasterService customerMasterService;

	RemoteCacheManager remoteCacheManager;
	@Inject
	@Remote("dashboard")
	RemoteCache<String, String> cache;

	@Inject
	ErrorBean errorBean;
	private static final String[] TABLE_NAME = { "customer_master", "address_master", "phone_master" };
	public static final String[] COLUMN_NAME = { "created_by", "updated_by", "created_date", "updated_date", "id",
			"customermaster_id", "customermasterhistory_id", "customerhistory_id", "version" };

	@Inject
	DataReportScheduler(RemoteCacheManager remoteCacheManager) {
		this.remoteCacheManager = remoteCacheManager;
	}

	/**
	 * This method Generate Report for Every table and every field based.
	 * 
	 * @param execution {@link ScheduledExecution}
	 */
	@SuppressWarnings("unchecked")
	@Scheduled(cron = "{data.report.cron.expr}")
	void cronJobWithExpressionInConfig(ScheduledExecution execution) {
		LOGGER.info("fire Cron expression configured in application.properties");
		LOGGER.info("fire Job Time :- " + execution.getScheduledFireTime());
		String tableNameQuery = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND TABLE_CATALOG='customer' ";
		String columnNameQuery = "SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name = ";
		Query query = em.createNativeQuery(tableNameQuery);
		List<String> listl = query.getResultList();
		for (String tableName : listl) {
			String columnQuery = columnNameQuery + "'" + tableName + "'";
			query = em.createNativeQuery(columnQuery);
			List<String> columnlist = query.getResultList();
			LOGGER.info(columnlist.toString());
			for (String columnName : columnlist) {
				if (StringUtils.containsAny(tableName, TABLE_NAME)
						&& !StringUtils.containsAny(columnName, COLUMN_NAME)) {
					LOGGER.info("Start Process " + tableName + " *** " + columnName);
					try {
						CustomerDataReport customerDataReport = customerMasterService
								.getCustomerDataReportCache(tableName, columnName);
						ObjectMapper Obj = new ObjectMapper();
						String jsonStr = Obj.writeValueAsString(customerDataReport);
						LOGGER.info("cache Object {} " + cache);
						cache.put(tableName + "-" + columnName, jsonStr);
						LOGGER.info("Save Success " + jsonStr);
					} catch (IOException e) {
						errorBean.createError(e, this.getClass().getName(), "cronJobWithExpressionInConfig");
						e.printStackTrace();
					}
				}
			}
		}
	}
}
