package com.mdm.app.common;

import java.time.LocalDateTime;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Component;

import com.mdm.app.entity.ErrorControl;

@Component
public class ErrorBean {

	@Transactional(value = TxType.REQUIRED)
	public void createError(Exception exception, String className, String methodName) {
		ErrorControl errorControl = new ErrorControl();
		errorControl.setError(exception.toString());
		errorControl.setClassName(className);
		errorControl.setMethodName(methodName);
		errorControl.setLine(exception.getStackTrace()[0].getLineNumber());
		errorControl.setCreatedBy("admin");
		errorControl.setCreatedDate(LocalDateTime.now());
		errorControl.persistAndFlush();
	}
}
