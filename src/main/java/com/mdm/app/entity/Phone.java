/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "phone_history")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class Phone extends PanacheEntity {

	@Column(name = "country_code")
	String countryCode;
	@Column(name = "extension")
	String extension;
	@Column(name = "phone_number")
	String phoneNumber;
	@Column(name = "phone_type")
	String phoneType;
	@Column(name = "preferred")
	String preferred;
	@ManyToOne
	@JsonbTransient
	private CustomerHistory customerHistory;

	@Override
	public String toString() {
		return "Phone [countryCode=" + countryCode + ", extension=" + extension + ", phoneNumber=" + phoneNumber
				+ ", phoneType=" + phoneType + ", preferred=" + preferred + "]";
	}

}
