package com.mdm.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "customer_datasources")
@Data
@EqualsAndHashCode(callSuper = false)
public class CustomerDataSources extends PanacheEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "source_name")
	String sourceName;
	@Column(name = "color_name")
	String colorName;
	@Column(name = "created_by")
	String createdBy;
	@Column(name = "updated_by")
	String updatedBy;
	@Column(name = "created_date")
	LocalDateTime createdDate;
	@Column(name = "updated_date")
	LocalDateTime updatedDate;

}