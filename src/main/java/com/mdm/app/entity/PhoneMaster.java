/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "phone_master")
@Indexed(index = "phonemaster")
@Data
@EqualsAndHashCode(callSuper = false)
public class PhoneMaster extends PanacheEntity {

	@Column(name = "country_code")
	@KeywordField(name = "country_code")
	String countryCode;
	@Column(name = "extension")
	@KeywordField(name = "extension")
	String extension;
	@Column(name = "phone_number")
	@KeywordField(name = "phone_number")
	String phoneNumber;
	@Column(name = "phone_type")
	@KeywordField(name = "phone_type")
	String phoneType;
	@Column(name = "preferred")
	@KeywordField(name = "preferred")
	String preferred;
	@ManyToOne
	@JsonbTransient
	private CustomerMaster customerMaster;

	@Override
	public String toString() {
		return "PhoneMaster [countryCode=" + countryCode + ", extension=" + extension + ", phoneNumber=" + phoneNumber
				+ ", phoneType=" + phoneType + ", preferred=" + preferred + "]";
	}

}
