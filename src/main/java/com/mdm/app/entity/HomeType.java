/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

public enum HomeType {
	MORTGAGE, RENT, OWN
}
