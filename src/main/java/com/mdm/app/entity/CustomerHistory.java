/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "customer_history")
@Data
@EqualsAndHashCode(callSuper = false)
public class CustomerHistory extends PanacheEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "customer_id")
	String customerId;
	@Column(name = "first_name")
	String firstName;
	@Column(name = "last_name")
	String lastName;
	@Column(name = "suffix")
	String suffix;
	@Column(name = "title")
	String title;
	@Column(name = "gender")
	String gender;
	@Column(name = "multi_chanel_communication_context")
	String multiChanelCommunicationContext;
	@Column(name = "employment")
	String employment;
	@Column(name = "birth_state")
	String birthState;
	@Column(name = "birth_country")
	String birthCountry;
	@Column(name = "maritual_status")
	String maritualStatus;
	@Column(name = "income_band")
	String incomeBand;
	@Column(name = "age_band")
	String ageBand;
	@Column(name = "has_children")
	String hasChildren;
	@Column(name = "internal_notes")
	String internalNotes;
	@Column(name = "email")
	String email;
	@Column(name = "from_source")
	String fromSource;

// Start Addtitional fields MDM-14
	@Column(name = "age")
	Integer age;
	@Column(name = "maritial_status")
	@KeywordField(name = "maritial_status")
	String maritialStatus;
	@Column(name = "education")
	String education;
	@Column(name = "grade")
	@KeywordField(name = "grade")
	String grade;
	@Column(name = "emp_title")
	@KeywordField(name = "emp_title")
	String empTitle;
	@Column(name = "emp_length")
	@KeywordField(name = "emp_length")
	String empLength;
	@Column(name = "home_owership")
	@KeywordField(name = "home_owership")
	@Enumerated(EnumType.STRING)
	HomeType homeOwnership;
	@Column(name = "zip_code")
	Long zipCode;
	@Column(name = "family_size")
	Integer familySize;
	@Column(name = "ssn")
	Long ssn;
	@Column(name = "sum_certificate_of_deposit")
	Long sumCertificateOfDeposit;
	@Column(name = "sum_credit_card")
	Long sumCreditCard;
	@Column(name = "sum_debit_card")
	Long sumDebitCard;
	@Column(name = "sum_international_debit")
	Long sumInternatinalDebit;
	@Column(name = "sum_saving_account")
	Long sumSavingsAccount;
	@Column(name = "sum_checking_account")
	Long sumCheckingAmount;
	@Column(name = "sum_vehicle_loan")
	Long sumVehicleLoan;
	@Column(name = "sum_personal_loan")
	Long sumPersonalLoan;
	@Column(name = "sum_Insurance")
	Long sumInsurance;
	@Column(name = "sum_securities")
	Long sumSecurities;
	@Column(name = "sum_overdraft")
	Long sumOverDraft;
// End  MDM-14

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "customerhistory_id")
	List<Address> address;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "customerhistory_id")
	List<Phone> phone;

	@Column(name = "created_by")
	String createdBy;
	@Column(name = "updated_by")
	String updatedBy;
	@Column(name = "created_date")
	LocalDateTime createdDate;
	@Column(name = "updated_date")
	LocalDateTime updatedDate;
}
