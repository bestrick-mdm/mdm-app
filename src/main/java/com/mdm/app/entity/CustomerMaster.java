/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.AssociationInverseSide;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexingDependency;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.ObjectPath;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.PropertyValue;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "customer_master")
@Indexed(index = "customermaster")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerMaster extends PanacheEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name = "customer_id")
	@KeywordField(name = "customer_id")
	String customerId;
	@FullTextField(analyzer = "name")
	@KeywordField(name = "firstName_sort", sortable = Sortable.YES, normalizer = "sort")
	@Column(name = "first_name")
	String firstName;
	@Column(name = "first_name_rule")
	@KeywordField(name = "first_name_rule")
	String firstNameRule;
	@Column(name = "first_name_source")
	@KeywordField(name = "first_name_source")
	String firstNameSource;
	@Column(name = "first_name_count")
	@KeywordField(name = "first_name_count")
	String firstNameCount;
	@Column(name = "last_name")
	@FullTextField(analyzer = "name")
	@KeywordField(name = "lastName_sort", sortable = Sortable.YES, normalizer = "sort")
	String lastName;
	@Column(name = "last_name_rule")
	String lastNameRule;
	@Column(name = "last_name_source")
	String lastNameSource;
	@Column(name = "last_name_count")
	String lastNameCount;
	@Column(name = "suffix")
	String suffix;
	@Column(name = "suffix_rule")
	String suffixRule;
	@Column(name = "suffix_source")
	String suffixSource;
	@Column(name = "suffix_count")
	String suffixCount;
	@Column(name = "title")
	String title;
	@Column(name = "title_rule")
	String titleRule;
	@Column(name = "title_source")
	String titleSource;
	@Column(name = "title_count")
	String titleCount;
	@Column(name = "gender")
	String gender;
	@Column(name = "gender_rule")
	String genderRule;
	@Column(name = "gender_source")
	String genderSource;
	@Column(name = "gender_count")
	String genderCount;
	@Column(name = "multi_chanel_communication_context")
	String multiChanelCommunicationContext;
	@Column(name = "multi_chanel_communication_context_rule")
	String multiChanelCommunicationContextRule;
	@Column(name = "multi_chanel_communication_context_source")
	String multiChanelCommunicationContextSource;
	@Column(name = "multi_chanel_communication_context_count")
	String multiChanelCommunicationContextCount;
	@Column(name = "employment")
	String employment;
	@Column(name = "employment_rule")
	String employmentRule;
	@Column(name = "employment_source")
	String employmentSource;
	@Column(name = "employment_count")
	String employmentCount;
	@Column(name = "birth_state")
	String birthState;
	@Column(name = "birth_state_rule")
	String birthStateRule;
	@Column(name = "birth_state_source")
	String birthStateSource;
	@Column(name = "birth_state_count")
	String birthStateCount;
	@Column(name = "birth_country")
	String birthCountry;
	@Column(name = "birth_country_rule")
	String birthCountryRule;
	@Column(name = "birth_country_source")
	String birthCountrySource;
	@Column(name = "birth_country_count")
	String birthCountryCount;
	@Column(name = "maritial_status")
	String maritialStatus;
	@Column(name = "maritial_status_rule")
	String maritialStatusRule;
	@Column(name = "maritial_status_count")
	String maritialStatusCount;
	@Column(name = "maritial_status_source")
	String maritialStatusSource;
	@Column(name = "income_band")
	String incomeBand;
	@Column(name = "income_band_rule")
	String incomeBandRule;
	@Column(name = "income_band_count")
	String incomeBandCount;
	@Column(name = "income_band_source")
	String incomeBandSource;
	@Column(name = "age_band")
	String ageBand;
	@Column(name = "age_band_rule")
	String ageBandRule;
	@Column(name = "age_band_count")
	String ageBandCount;
	@Column(name = "age_band_source")
	String ageBandSource;
	@Column(name = "has_children")
	String hasChildren;
	@Column(name = "has_children_rule")
	String hasChildrenRule;
	@Column(name = "has_children_count")
	String hasChildrenCount;
	@Column(name = "has_children_source")
	String hasChildrenSource;
	@Column(name = "internal_notes")
	String internalNotes;
	@Column(name = "internal_notes_rule")
	String internalNotesRule;
	@Column(name = "internal_notes_count")
	String internalNotesCount;
	@Column(name = "internal_notes_source")
	String internalNotesSource;
	@FullTextField(analyzer = "email")
	@KeywordField(name = "email_sort", sortable = Sortable.YES, normalizer = "sort")
	@Column(name = "email")
	String email;
	@Column(name = "email_rule")
	String emailRule;
	@Column(name = "email_source")
	String emailSource;
	@Column(name = "email_count")
	String emailCount;
	@Column(name = "from_source")
	String fromSource;
// Start Addtitional fields MDM-14
	@Column(name = "age")
	Integer age;
	
	@Column(name = "education")
	String education;
	@Column(name = "grade")
	@KeywordField(name = "grade")
	String grade;
	@Column(name = "emp_title")
	@KeywordField(name = "emp_title")
	String empTitle;
	@Column(name = "emp_length")
	@KeywordField(name = "emp_length")
	String empLength;
	@Column(name = "home_owership")
	@KeywordField(name = "home_owership")
	@Enumerated(EnumType.STRING)
	HomeType homeOwnership;
	@Column(name = "zip_code")
	Long zipCode;
	@Column(name = "family_size")
	Integer familySize;
	@Column(name = "ssn")
	Long ssn;
	@Column(name = "sum_certificate_of_deposit")
	Long sumCertificateOfDeposit;
	@Column(name = "sum_credit_card")
	Long sumCreditCard;
	@Column(name = "sum_debit_card")
	Long sumDebitCard;
	@Column(name = "sum_international_debit")
	Long sumInternatinalDebit;
	@Column(name = "sum_saving_account")
	Long sumSavingsAccount;
	@Column(name = "sum_checking_account")
	Long sumCheckingAmount;
	@Column(name = "sum_vehicle_loan")
	Long sumVehicleLoan;
	@Column(name = "sum_personal_loan")
	Long sumPersonalLoan;
	@Column(name = "sum_Insurance")
	Long sumInsurance;
	@Column(name = "sum_securities")
	Long sumSecurities;
	@Column(name = "sum_overdraft")
	Long sumOverDraft;
// End  MDM-14

	@IndexedEmbedded
	@IndexingDependency(reindexOnUpdate = ReindexOnUpdate.NO)
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@AssociationInverseSide(inversePath = @ObjectPath(@PropertyValue(propertyName = "address")))
	@JoinColumn(name = "customermaster_id")
	List<AddressMaster> address;

	@IndexedEmbedded
	@IndexingDependency(reindexOnUpdate = ReindexOnUpdate.NO)
	@AssociationInverseSide(inversePath = @ObjectPath(@PropertyValue(propertyName = "phone")))
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "customermaster_id")
	List<PhoneMaster> phone;

	@Column(name = "created_by")
	@KeywordField(name = "created_by")
	@JsonIgnore
	String createdBy;
	@Column(name = "updated_by")
	@KeywordField(name = "updated_by")
	@JsonIgnore
	String updatedBy;
	@Column(name = "created_date")
	@JsonIgnore
	LocalDateTime createdDate;
	@Column(name = "updated_date")
	@JsonIgnore
	LocalDateTime updatedDate;
// Addtional Fields for tracking records history
	@Column(name = "address_rule")
	@KeywordField(name = "address_rule")
	String addressRule;
	@Column(name = "address_source")
	@KeywordField(name = "address_source")
	String addressSource;
	@Column(name = "phone_rule")
	@KeywordField(name = "phone_rule")
	String phoneRule;
	@Column(name = "phone_source")
	@KeywordField(name = "phone_source")
	String phoneSource;
}
