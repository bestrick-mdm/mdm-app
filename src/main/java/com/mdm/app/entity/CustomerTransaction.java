/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "customer_transaction")
@Indexed(index = "customertransaction")
@Data
@EqualsAndHashCode(callSuper = false)
public class CustomerTransaction extends PanacheEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(name = "customer_id")
	@KeywordField(name = "customer_id")
	String customerId;
	@Column(name = "account_id")
	@KeywordField(name = "account_id")
	String accountId;
	@Column(name = "type")
	@KeywordField(name = "type")
	@Enumerated(EnumType.STRING)
	AccountType type;
	@Column(name = "description")
	@KeywordField(name = "description")
	String description;
	@Column(name = "approved_amount")
	Integer approvedAmount;
	@Column(name = "credit")
	Integer credit;
	@Column(name = "debit")
	Integer debit;
}
