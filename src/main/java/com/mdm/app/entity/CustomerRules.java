/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "customer_rules")
@Data
@EqualsAndHashCode(callSuper = false)
public class CustomerRules extends PanacheEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "field_name")
	String fieldName;
	@Column(name = "rule_name")
	String ruleName;
	@Column(name = "from_source")
	String fromSource;
	@Column(name = "priority")
	int priority;
	@Column(name = "matching_weight")
	float matchingWeight;
	@Column(name = "created_by")
	String createdBy;
	@Column(name = "updated_by")
	String updatedBy;
	@Column(name = "created_date")
	LocalDateTime createdDate;
	@Column(name = "updated_date")
	LocalDateTime updatedDate;

}