/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "address_history")
@Data
@EqualsAndHashCode(callSuper = false)
public class Address extends PanacheEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "street_1")
	String street_1;
	@Column(name = "street_2")
	String street_2;
	@Column(name = "apt_suite")
	String aptSuite;
	@Column(name = "city")
	String city;
	@Column(name = "state")
	String state;
	@Column(name = "zip")
	String zip;
	@Column(name = "country")
	String country;
	@Column(name = "preferred")
	String preferred;
	@ManyToOne
	@JsonbTransient
	private CustomerHistory customerHistory;

	@Override
	public String toString() {
		return "Address [street_1=" + street_1 + ", street_2=" + street_2 + ", aptSuite=" + aptSuite + ", city=" + city
				+ ", state=" + state + ", zip=" + zip + ", country=" + country + ", preferred=" + preferred + "]";
	}

}
