/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class CustomerData implements Serializable {

	private static final long serialVersionUID = 1L;
	CustomerHistory customer;

}
