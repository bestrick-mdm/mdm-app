/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "phone_master_history")
@Data
@EqualsAndHashCode(callSuper = false)
public class PhoneMasterHistory extends PanacheEntity {

	@Column(name = "country_code")
	String countryCode;
	@Column(name = "extension")
	String extension;
	@Column(name = "phone_number")
	String phoneNumber;
	@Column(name = "phone_type")
	String phoneType;
	@Column(name = "preferred")
	String preferred;
	@ManyToOne
	private CustomerMasterHistory customerMasterHistory;

	@Override
	public String toString() {
		return "PhoneMasterHistory [countryCode=" + countryCode + ", extension=" + extension + ", phoneNumber="
				+ phoneNumber + ", phoneType=" + phoneType + ", preferred=" + preferred + "]";
	}

}
