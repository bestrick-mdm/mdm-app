/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "customer_Ids")
@Data
@ToString
@EqualsAndHashCode(callSuper=false)
public class CustomerIdMapper extends PanacheEntity {

	@Column(name = "pri_customer_id")
	String primaryCustomerId; // map customer id
	@Column(name = "sec_customer_id")
	String secondaryCustomerId; // non - consistent customer id
	@Column(name = "sec_source")
	String secondarySource;
}
