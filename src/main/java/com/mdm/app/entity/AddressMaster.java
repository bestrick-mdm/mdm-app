/*
 *  Copyright MDM (c) 2020 Project MDM
 *
 *  All rights reserved.
 */
package com.mdm.app.entity;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "address_master")
@Indexed(index = "addressmaster")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AddressMaster extends PanacheEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "street_1")
	@KeywordField(name = "street_1")
	String street_1;
	@Column(name = "street_2")
	@KeywordField(name = "street_2")
	String street_2;
	@Column(name = "apt_suite")
	@KeywordField(name = "apt_suite")
	String aptSuite;
	@Column(name = "city")
	@KeywordField(name = "city")
	String city;
	@Column(name = "state")
	@KeywordField(name = "state")
	String state;
	@Column(name = "zip")
	@KeywordField(name = "zip")
	String zip;
	@Column(name = "country")
	@KeywordField(name = "country")
	String country;
	@Column(name = "preferred")
	@KeywordField(name = "preferred")
	String preferred;
	@ManyToOne
	@JsonbTransient
	@JsonIgnore
	private CustomerMaster customerMaster;

	@Override
	public String toString() {
		return "AddressMaster [street_1=" + street_1 + ", street_2=" + street_2 + ", aptSuite=" + aptSuite + ", city="
				+ city + ", state=" + state + ", zip=" + zip + ", country=" + country + ", preferred=" + preferred
				+ "]";
	}

}
