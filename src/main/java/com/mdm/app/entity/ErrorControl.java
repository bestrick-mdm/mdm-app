package com.mdm.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "error_handling")
@Data
@EqualsAndHashCode(callSuper = false)
public class ErrorControl extends PanacheEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "error")
	private String error;
	@Column(name = "className")
	private String className;
	@Column(name = "methodName")
	private String methodName;
	@Column(name = "line")
	private int line;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "created_date")
	private LocalDateTime createdDate;

}
