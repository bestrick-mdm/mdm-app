package com.mdm.app;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class CustomerResourceTest {

	@Test
	public void testCustomerListEndpoint() {
		given().when().get("/customer/history").then().statusCode(200);
	}
	
	@Test
	public void testCustomerDataSourcesEndpoint() {
		given().when().get("/customer/history/sources/M0001").then().statusCode(200);
	}
}